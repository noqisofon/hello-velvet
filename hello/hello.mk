outfile-extension     := .exe
project-name          := hello
intermediate-dir      := ./obj/Debug
out-dir               := ./bin/Debug
target                := $(out-dir)/$(project-name)$(outfile-extension)

##
## Common Variables
##
AR         := ar rcus
MKDIR      := mkdir
TOUCH      := touch
CXX        := g++
LDCXX      := g++
CC         := gcc
LDCC       := gcc

CXXFLAGS   := -g -O2 -W -Wall
LDCXXFLAGS := 
CFLAGS     := -g -O2 -W -Wall
LDCCFLAGS  := 

##
## User defined environment variables
##
src0       := main.c

obj0       := $(intermediate-dir)/main.o
objs       := $(obj0)

##
## Main Build Targets
##
.PHONY : all run clean prebuild postbuild make-intermediate-dirs
all: $(target)

$(target): $(objs)
	$(LDCC) -o $@ $^ $(LDCCFLAGS)

$(obj0): $(src0)
	$(CC) -o $@ -c $^ $(CFLAGS)

run: $(target)
	$(target)

clean:
	@$(RM) -v $(objs) $(target)

prebuild: make-intermediate-dirs

postbuild:

make-intermediate-dirs: $(intermediate-dir)/.dir $(out-dir)/.dir

$(intermediate-dir)/.dir:
	@$(MKDIR) -p $(intermediate-dir)
	@$(TOUCH) $(intermediate-dir)/.dir

$(out-dir)/.dir:
	@$(MKDIR) -p $(out-dir)
	@$(TOUCH) $(out-dir)/.dir
