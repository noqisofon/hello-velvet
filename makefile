.PHONY : all run clean

all:
	@echo " ---- Building project hello - Debug"
	make -C ./hello -f ./hello.mk prebuild
	make -C ./hello -f ./hello.mk all
	make -C ./hello -f ./hello.mk postbuild
	@echo " ---- "

run:
	make -C ./hello -f ./hello.mk run

clean:
	@echo " ---- Cleaning project hello - Debug"
	make -C ./hello -f ./hello.mk clean
	@echo " ---- "
